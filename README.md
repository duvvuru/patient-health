# Patients Health
Test for patients health. "MVP". Focus was on the MVP, position to be able to iterate and enhance. 

## Developer Setup

###Pre-requisite
Install node.js 

### How to use

    npm install
    npm start

Test page available at `http://localhost:3000`
    
### Unit test

    npm test
    
    
## Notes

### Choice of technology
I could have gone with server side api with Java using spring boot. 
For UI I could have gone with any of the frameworks listed. 


Since you might know the other ui frameworks I thought of demoing development with a different technology.
Webcomponents using google polymer. It might be fun to see how it works.

Since this is the test, speed of development is more important. Node.js is fast to develop with and I don't have to 
worry about the concurrency. Performance should be very good for the demo. 
 
### UI 
 
I stayed with a very basic UI. It's easy to style the UI to our needs depending on the UX. We can go with bootstrap or
expand on the material design. 
 
Tables are very basic. They can be easily expanded to provide sort/filter. 
  
Current UI is not to my liking but didn't want to spend extra effort to make it pretty. For MVP I feel like this should
 be fine.
 
 
Probably an hr more can get the UI responsive as well with good styling.


### Server

See TODO for enhancements that we can iterate on. I added few sample unit tests as well. 

### Database

I used the embedded database [nedb](https://github.com/louischatriot/nedb). Syntax is very similar to MongoDB. 
I choose this to avoid the overhead of a database service. nedb is lightweight and you should get the idea of the syntax. 
Obviously this cannot be used in server side production. The distributed servers need to have the nosql database like Mongo 
or Dynamo or Cassandra or Couch or Elastic. nedb may be fine to use on the client side. 

### Other

Many were considered out of scope. Continuous integration and deployment. Docker container and kubernetes configuration to 
orchestrate container. UI robot automation tests. Configurations for different environments. Authentication and authorization. 
Api versioning. to name few
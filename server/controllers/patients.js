const db = require('../db/db.js');

module.exports = {

    /*
    * Get's all patients;
    * TODO: can add pagination
    */
    getAll: function (req, res) {

        db.find({}, function(err, data) {
            if(err) {
                console.error(err); //TODO add bunyan logger
                res.status(500).json(err); //TODO: can mask error
                return;
            }
            res.json(data);
        });
    },

    getOne: function (req, res) {
        let patientId = req.params.id;
        db.findOne({"_id": parseInt(patientId)}, function (err, data) {
            if(err) {
                console.error(err); //TODO add bunyan logger
                res.status(500).json(err);
                return;
            }
            if(data) {
                res.json(data); //TODO only send enough data
            } else {
                res.sendStatus(404);
            }
        });
    },

    /*
    * Gets medications for a patient
    * TODO: Move this to medications service
    */
    getMedications: function(req, res) {
        let patientId = req.params.id;
        db.findOne({"_id": parseInt(patientId)}, function (err, data) {
            if(err) {
                console.error(err); //TODO add bunyan logger
                res.status(500).json(err);
                return;
            }
            if(data && data.medications) {
                res.json(data.medications);
            } else {
                res.sendStatus(404);
            }
        });
    },

    /*
     * Gets vitals for a patient
     * TODO: Move this to vitals service
     */
    getVitals: function(req, res){
        let patientId = req.params.id;
        db.findOne({"_id": parseInt(patientId)}, function (err, data) {
            if(err) {
                console.error(err); //TODO add bunyan logger
                res.status(500).json(err);
                return;
            }
            if(data && data.vitals) {
                res.json(data.vitals);
            } else {
                res.sendStatus(404);
            }
        });
    },

    /*
     * puts vitals for a patient; From the doc patient has only one value stored
     * TODO: can be expanded to have history of temperature and pulse
     * TODO: Can be split to be able to patch only temperature or pulse
     *      ex: PATCH /patients/:id/vitals-temperature
      *         PATCH /patients/:id/vitals-pulse
     */
    putVitals: function (req,res) {

        if(!req.body || Object.keys(req.body).length === 0) {
            return res.sendStatus(400);
        }

        let patientId = req.params.id;
        let body = req.body;

        //TODO: convert this into value and units;
        //TODO: add input validation
        //TODO: add timestamps for inserted/updated

        let vitals = {
            temperature: body.temperature,
            pulse: body.pulse
        };

        db.update({"_id": parseInt(patientId)}, {$set: {vitals: vitals}}, function (err, data) {
            if(err) {
                console.error(err); //TODO add bunyan logger
                res.status(500).json(err);
                return;
            }
            res.sendStatus(200);
        });
    }


};
// Node.js notation for importing packages
var express = require('express');
var patients = require('./server/controllers/patients.js');
var bodyParser = require('body-parser')

// Spin up a server
var app = express();


// Serve static files from the main directory
//app.use(express.static(__dirname + '/build/bundled'));
app.use(express.static(__dirname + '/build/bundled'));
app.use('/client', express.static('client'));
app.use('/bower_components', express.static('bower_components'));

app.use(bodyParser.json());

//  ____________________________________
// |                                    |
// |       Setting up Page Routes       |
// |____________________________________|
//

app.get('/', function(req, res){
    res.sendFile("index.html", {root: '.'});
});

app.get('/patients', patients.getAll);
app.get('/patients/:id(\\d+)', patients.getOne);
app.get('/patients/:id(\\d+)/medications', patients.getMedications); //TODO: can add medication service
app.get('/patients/:id(\\d+)/vitals', patients.getVitals); //TODO: can add vitals service
app.put('/patients/:id(\\d+)/vitals', patients.putVitals); //TODO: can add vitals service


// Tell the app to listen for requests on port 3000
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../server.js');
let should = chai.should();

chai.use(chaiHttp);


const assert = require('assert');

describe('Patients', function() {

    /*
    beforeEach(function(done) {
        // I do stuff like populating db
    });

    afterEach(function(done) {
        // I do stuff like deleting populated db
    });
    */

    describe('#patients', function() {
        it('get should return all patients', function(done) {
            chai.request(server)
                .get('/patients')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(2);
                    done();
                });
        });

        it('get one should return one patient', function(done) {
            chai.request(server)
                .get('/patients/1')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('get one if not found should return 404', function(done) {
            chai.request(server)
                .get('/patients/3')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('#medications', function() {
        it('get should return medications for a patient', function(done) {
            chai.request(server)
                .get('/patients/1/medications')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(3);
                    done();
                });
        });

        it('get should return 404 for medications not found', function(done) {
            chai.request(server)
                .get('/patients/3/medications')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });

        it('get should return 404 for invalid patient id', function(done) {
            chai.request(server)
                .get('/patients/foo/medications')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('#vitals', function() {
        it('get should return vitals for a patient', function(done) {
            chai.request(server)
                .get('/patients/1/vitals')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('put should return 200 on success', function(done) {
            let vitals = {temperature: '96F', pulse: '120bpm'};
            chai.request(server)
                .put('/patients/1/vitals')
                .send(vitals)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                // }).then(function () {
                //    chai.request(server)
                //        .get('/patients/1/vitals')
                //        .end((err, res) => {
                //           res.should.have.status(200);
                //           res.body.should.be.eql(vitals);
                //           done();
                //       });
                 });

        });

        it('put no body should return bad request', function(done) {
            chai.request(server)
                .put('/patients/1/vitals')
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });
});